import { ref, computed } from "vue";
import { defineStore } from "pinia";
import type Customer from "@/types/Customer";
import customerService from "@/services/customer";

export const useCustomerStore = defineStore("customer", () => {
  const customers = ref<Customer[]>([]);
  async function getCustomers() {
    try {
      const res = await customerService.getCustomers();
      customers.value = res.data;
      console.log(res);
    } catch (e) {
      console.log(e);
    }
  }
  return { customers, getCustomers };
});
