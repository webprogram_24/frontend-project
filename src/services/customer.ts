import http from "./axios";

function getCustomers() {
  return http.get("/customers");
}

// function saveCustomer(product: Product) {
//   return http.post("/products", product);
// }

// function updateProduct(id: number, product: Product) {
//   return http.patch(`/products/${id}`, product);
// }

// function deleteProduct(id: number) {
//   return http.delete(`/products/${id}`);
// }

export default { getCustomers };
